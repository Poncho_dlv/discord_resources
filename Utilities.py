#!/usr/bin/env python
# -*- coding: utf-8 -*-


import asyncio
import logging
from datetime import datetime
from enum import IntEnum

import discord
from pytz import timezone

from discord_resources.Resources import Resources
from spike_database.DiscordGuildInfo import DiscordGuildInfo
from spike_database.ResourcesRequest import ResourcesRequest
from spike_exception import DefaultError
from spike_model.Competition import Competition
from spike_model.League import League
from spike_requester.CyanideApi import CyanideApi
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_settings.SpikeSettings import SpikeSettings
from spike_translation import Translator
from spike_utilities.Utilities import Utilities

spike_logger = logging.getLogger("spike_logger")

PONCHO_DISCORD_ID = 315120413883760640


class TypeInfo(IntEnum):
    STRING = 0
    DISCORD_ID = 1
    FILE = 3
    DATE_TIME = 4


async def update_guild(guild):
    discord_guild_db = DiscordGuildInfo()
    excluded_server = Utilities.get_excluded_server()
    excluded_server.extend(SpikeSettings.get_server_to_hide())
    try:
        if guild is not None and guild.id not in excluded_server:
            spike_logger.info(f"Update: {guild.name} - {guild.id}")
            try:
                if guild.system_channel is not None:
                    channel_id = guild.system_channel.id
                else:
                    channel_id = guild.text_channels[0].id
                channel = guild.get_channel(channel_id)
                invitation = await channel.create_invite(reason="Invite created by Spike!", unique=False)
            except discord.errors.Forbidden:
                spike_logger.info(f"Invitation forbidden in guild: {guild.name} - {guild.id}")
                invitation = None

            text_channels = []
            roles = []

            for channel in guild.text_channels:
                if channel is not None:
                    channel_data = {
                        "id": str(channel.id),
                        "name": channel.name
                    }
                    text_channels.append(channel_data)

            for role in guild.roles:
                if role is not None:
                    role_data = {
                        "name": role.name,
                        "id": str(role.id),
                        "position": role.position
                    }
                    roles.append(role_data)
            if guild.icon is not None:
                icon_url = guild.icon.url
            else:
                icon_url = 'https://cdn.discordapp.com/emojis/469828281416286229.png'
            guild_data = {
                "discord_id": str(guild.id),
                "name": guild.name,
                "icon": icon_url,
                "text_channels": text_channels,
                "owner_id": str(guild.owner_id),
                "description": guild.description,
                "roles": roles
            }

            if invitation is not None:
                guild_data["invite"] = invitation.code
            discord_guild_db.update_guild_info(guild_data)
    except Exception as e:
        spike_logger.info("Ignore guild: {} - {}".format(guild.name, guild.id))
        spike_logger.error(e)


class DiscordUtilities:

    def __init__(self, client):
        self.client = client

    @staticmethod
    def get_members_by_role(role_id, guild):
        output = []
        for member in guild.members:
            for member_role in member.roles:
                if member_role.id == role_id:
                    output.append(member)
        return output

    @staticmethod
    def __is_allowed(member: discord.Member, role_needed: int):
        for role in member.roles:
            if role.id == role_needed:
                return True
        return False

    def is_bot_admin(self, ctx):
        if ctx.author.guild_permissions.administrator or ctx.author.id == PONCHO_DISCORD_ID:
            return True

        settings = DiscordGuildSettings(f"{ctx.guild.id}")
        admin_role = settings.get_bot_admin_role()
        return self.__is_allowed(ctx.author, admin_role)

    def member_is_bot_admin(self, member: discord.Member, server_id: int):

        if member.guild_permissions.administrator or member.id == PONCHO_DISCORD_ID:
            return True
        settings = DiscordGuildSettings(server_id)
        admin_role = settings.get_bot_admin_role()
        return self.__is_allowed(member, admin_role)

    @staticmethod
    async def send_custom_message(channel, message=None, emoji=None, embed=None, raise_error=True, send_typing=True):
        msg = None
        try:
            if channel is not None:
                if send_typing:
                    await channel.typing()
                msg = await channel.send(content=message, embed=embed)
                if emoji is not None:
                    if isinstance(emoji, list):
                        for emo in emoji:
                            await msg.add_reaction(emo)
                    else:
                        await msg.add_reaction(emoji)
            else:
                spike_logger.error("send_custom_message: None channel")
        except Exception as e:
            spike_logger.error("send_custom_message_error: {}".format(e))
            if raise_error:
                raise
        return msg

    @staticmethod
    async def delete_messages(messages: list):
        for message in messages:
            await DiscordUtilities.delete_message(message)

    @staticmethod
    async def delete_message(message: discord.Message):
        if message is not None and isinstance(message, discord.Message) and isinstance(message.channel, discord.TextChannel):
            try:
                await message.delete()
            except discord.Forbidden:
                spike_logger.error("Spike need message management permission in {channel_name} channel.".format(channel_name=message.channel.name))
            except discord.NotFound:
                spike_logger.debug("Unable to delete, message not found")
            except discord.HTTPException as e:
                spike_logger.error("HTTPException {}".format(e))
            except Exception as e:
                spike_logger.error("Error in delete_message {}".format(e))

    async def ask_user(self, type_info: TypeInfo, request: str, description: str, cancel_word: str, channel, user, timeout: int = 1):
        user_reply = None
        embed = discord.Embed(title=request, description=description, color=0x992d22)
        msg1 = await self.send_custom_message(channel, embed=embed)
        list_of_msg_to_delete = [msg1]

        def check(msg):
            if msg.author == user and msg.channel == channel:
                list_of_msg_to_delete.append(msg)
                if msg.content == cancel_word:
                    return True
                else:
                    if type_info == TypeInfo.DISCORD_ID:
                        try:
                            reply = Utilities.get_discord_id(msg.content)
                            if self.client.get_channel(reply) is None:
                                return False
                        except ValueError:
                            return False
                        else:
                            return True
                    elif type_info == TypeInfo.STRING:
                        return True
                    elif type_info == TypeInfo.FILE:
                        return len(msg.attachments) > 0
                    else:
                        return False
            else:
                return False
        try:
            response_msg = await self.client.wait_for("message", check=check, timeout=timeout * 60)
        except asyncio.TimeoutError:
            settings = DiscordGuildSettings(channel.guild.id)
            if timeout == 1:
                # /* Reply timeout: You have 1 minute to reply */
                raise DefaultError(Translator.tr("#_discord_utils.reply_timeout_1_min", settings.get_language()))
            else:
                # /* Reply timeout: You have {timeout} minutes to reply */
                raise DefaultError(Translator.tr("#_discord_utils.reply_timeout_min", settings.get_language()).format(timeout=timeout))
        else:
            if response_msg.content != cancel_word:
                if type_info == TypeInfo.DISCORD_ID:
                    user_reply = Utilities.get_discord_id(response_msg.content)
                elif type_info == TypeInfo.STRING:
                    user_reply = response_msg.content
                elif type_info == TypeInfo.FILE:
                    if len(response_msg.attachments) > 0:
                        user_reply = response_msg.attachments[0]

        await self.delete_messages(list_of_msg_to_delete)
        return user_reply

    async def ask_channel_id(self, request: str, description: str, cancel_word: str, channel, user, timeout: int = 1):
        channel_id = None
        embed = discord.Embed(title=request, description=description, color=0x992d22)
        msg1 = await self.send_custom_message(channel, embed=embed)
        list_of_msg_to_delete = [msg1]

        def check(msg):
            if msg.author == user and msg.channel == channel:
                list_of_msg_to_delete.append(msg)
                if msg.content == cancel_word:
                    return True
                else:
                    try:
                        reply = Utilities.get_discord_id(msg.content)
                        if self.client.get_channel(reply) is None:
                            return False
                    except ValueError:
                        return False
                    else:
                        return True
            else:
                return False

        try:
            response_msg = await self.client.wait_for("message", check=check, timeout=timeout * 60)
        except asyncio.TimeoutError:
            settings = DiscordGuildSettings(channel.guild.id)
            if timeout == 1:
                # /* Reply timeout: You have 1 minute to reply */
                raise DefaultError(Translator.tr("#_discord_utils.reply_timeout_1_min", settings.get_language()))
            else:
                # /* Reply timeout: You have {timeout} minutes to reply */
                raise DefaultError(Translator.tr("#_discord_utils.reply_timeout_min", settings.get_language()).format(timeout=timeout))
        else:
            if response_msg.content != cancel_word:
                channel_id = Utilities.get_discord_id(response_msg.content)

        await self.delete_messages(list_of_msg_to_delete)
        return channel_id

    async def ask_string(self, request: str, description: str, cancel_word: str, channel, user, timeout: int = 1):
        str_ret = None
        embed = discord.Embed(title=request, description=description, color=0x992d22)
        msg1 = await self.send_custom_message(channel, embed=embed)
        list_of_msg_to_delete = [msg1]

        def check(msg):
            if msg.author == user and msg.channel == channel:
                list_of_msg_to_delete.append(msg)
                if msg.content != "":
                    return True
                return False
            else:
                return False

        try:
            response_msg = await self.client.wait_for("message", check=check, timeout=timeout * 60)
        except asyncio.TimeoutError:
            settings = DiscordGuildSettings(channel.guild.id)
            if timeout == 1:
                # /* Reply timeout: You have 1 minute to reply */
                raise DefaultError(Translator.tr("#_discord_utils.reply_timeout_1_min", settings.get_language()))
            else:
                # /* Reply timeout: You have {timeout} minutes to reply */
                raise DefaultError(Translator.tr("#_discord_utils.reply_timeout_min", settings.get_language()).format(timeout=timeout))
        else:
            if response_msg.content.lower() != cancel_word:
                str_ret = response_msg.content

        await self.delete_messages(list_of_msg_to_delete)
        return str_ret

    # TODO move cal feature to website
    async def ask_date_time(self, message, request_message, channel, user, date_formats, guild_tz="UTC"):
        date_time = None
        date_format = None
        description = request_message
        description += "\n\nTimezone **{}**\nReply format:\n DD/MM :: HH:MM\n DD/MM :: ??:??\n DD/MM :: HH:??\n\nEnter *exit* to cancel.".format(guild_tz)

        embed = message.embeds[0]
        embed.description = description
        await message.edit(embed=embed)
        list_of_msg_to_delete = []

        def check(msg):
            if msg.author == user and msg.channel == channel:
                list_of_msg_to_delete.append(msg)
                if msg.content == "exit":
                    return True
                else:
                    match = False
                    for current_date_format in date_formats:
                        try:
                            datetime.strptime(msg.content, current_date_format)
                            match = True
                        except ValueError:
                            pass
                    return match
            else:
                return False

        try:
            response_msg = await self.client.wait_for("message", check=check, timeout=120.0)
        except asyncio.TimeoutError:
            rsrc = Resources()
            settings = DiscordGuildSettings(channel.guild.id)
            await self.send_custom_message(channel, message=Translator.tr("#_discord_utils.reply_timeout_min", settings.get_language()).format(timeout=2), emoji=rsrc.get_remove_emoji())
        else:
            if response_msg.content != "exit":
                for df in date_formats:
                    try:
                        input_tz = timezone(guild_tz)
                        date_time = datetime.strptime(response_msg.content, df)

                        dt_now = datetime.now(timezone(guild_tz))
                        if date_time.month < dt_now.month:
                            year = dt_now.year + 1
                        else:
                            year = dt_now.year
                        date_time = date_time.replace(year=year, second=0, microsecond=0)
                        date_time = input_tz.localize(date_time, True)
                        date_format = df
                        break
                    except ValueError:
                        pass

        await self.delete_messages(list_of_msg_to_delete)
        return date_time, date_format

    @staticmethod
    def display_bounty(bounty_data, server_id):
        settings = DiscordGuildSettings(server_id)
        admin_bounty_only = settings.get_display_admin_bounty_only()
        guild_db = DiscordGuildInfo()

        if server_id is None or not admin_bounty_only:
            return True

        guild_data = guild_db.get_guild_data(str(server_id))
        for admin in guild_data.get("admins", []):
            if admin.get("id") in bounty_data["issuers"]:
                return True
        return False

    @staticmethod
    def get_default_competition_data(league_name, competition_name, platform, channel):
        rsrc = Resources()
        common_db = ResourcesRequest()
        competition_data = CyanideApi.get_competitions(league_name, platform=platform)

        if competition_data is not None and competition_data["competitions"] is not None:
            ingame_competitions = competition_data["competitions"]
            for current_competition in ingame_competitions:
                current_competition = Competition(current_competition)
                if current_competition.get_name() == competition_name:
                    data = [1, current_competition.get_id(), current_competition.get_name(), channel.id,
                            common_db.get_team_emoji_url(current_competition.get_league().get_logo()),
                            common_db.get_team_emoji(current_competition.get_league().get_logo()), platform,
                            current_competition.get_league().get_id()]
                    return data

        # No data found league or competition deleted
        league_data = CyanideApi.get_league(league_name, platform)
        if league_data is not None and league_data["league"] is not None:
            league = League(league_data["league"])
            default_emoji = common_db.get_team_emoji(league.get_logo())
            default_emoji_url = common_db.get_team_emoji_url(league.get_logo())
        else:
            # League and competition probably deleted
            default_emoji = rsrc.get_blood_bowl_2_emoji()
            default_emoji_url = "http://images.bb2.cyanide-studio.com/misc/II.png"

        # Build fake data manually
        data = [1, 1, competition_name, channel.id, default_emoji_url, default_emoji, platform, 1]
        return data

    def get_video_channel(self, title, server_id, live=False):
        settings = DiscordGuildSettings(server_id)

        if title is not None:
            lower_title = title.lower()
            bb_keywords = ["blood bowl", "fumbbl", "bb", "BloodBowl"]
            try:
                if live:
                    live_channel = settings.get_live_channel()
                    if live_channel != -1:
                        return self.client.get_channel(live_channel)
                    else:
                        return None
            except Exception as e:
                spike_logger.debug("Error in get_video_channel (live): {}".format(e))

            try:
                specific_channel = settings.get_specific_channel()
                keywords = settings.get_specific_channel_keywords()
                if type(keywords) == str:
                    keywords = keywords.lower()
                    keywords = keywords.split(",")

                if len(keywords) > 0:
                    for key in keywords:
                        if key.strip() in lower_title:
                            if specific_channel != -1:
                                return self.client.get_channel(specific_channel)
                            else:
                                return None
            except Exception as e:
                spike_logger.error("Error in get_video_channel: {}".format(e))
                spike_logger.error("No specific channel or keywords specified")

            try:
                bb_channel = settings.get_bb_channel()
                other_channel = settings.get_other_channel()

                for key in bb_keywords:
                    if key in lower_title:
                        if bb_channel != -1:
                            return self.client.get_channel(bb_channel)
                        else:
                            return None
                if other_channel != -1:
                    return self.client.get_channel(other_channel)
                else:
                    return None

            except Exception as e:
                spike_logger.error("Error in get_video_channel: {}".format(e))
                spike_logger.error("No BB or other channel specified")
                raise
        else:
            spike_logger.error("Error in get_video_channel")
            spike_logger.error("None title")
