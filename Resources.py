#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Resources:

    @staticmethod
    def get_blood_bowl_2_emoji():
        return "<:II:466589965707640843>"

    @staticmethod
    def get_remove_emoji():
        return "🗑"

    @staticmethod
    def get_calendar_emoji():
        return "📅"

    @staticmethod
    def get_link_emoji():
        return "🔗"

    @staticmethod
    def get_bounty_emoji():
        return "<:bounty:480775725939425310>"

    @staticmethod
    def get_more_details_emoji():
        return "<:More:507847486053023755>"

    @staticmethod
    def get_mvp_emoji():
        return "<:MVPCondition:469827205493686273>"

    @staticmethod
    def get_gold_trophy():
        return "<:BB2_Trophy_Gold:469825960456486932>"

    @staticmethod
    def get_silver_trophy():
            return "<:BB2_Trophy_Silver:469825959906770955>"

    @staticmethod
    def get_bronze_trophy():
        return "<:BB2_Trophy_Bronze:469825960406155264>"

    @staticmethod
    def get_edit_coach():
        return "<:EditUser:537249996320407562>"

    @staticmethod
    def get_remove_coach():
        return "<:EditUser:537249996320407562>"

    @staticmethod
    def get_search_coach():
        return "<:SearchUser:537249995808571421>"

    @staticmethod
    def get_valid_coach():
        return "<:AcceptUser:537249995322294273>"

    @staticmethod
    def get_help_coach():
        return "<:UserHelp:537249996337315850>"

    @staticmethod
    def get_twitch_emoji():
        return "<:twitch:525264526363590657>"

    @staticmethod
    def get_twitch_edit_emoji():
        return "<:twitch_edit:540944914482200616>"

    @staticmethod
    def get_youtube_emoji():
        return "<:youtube:525264526640283658>"

    @staticmethod
    def get_youtube_edit_emoji():
        return "<:youtube_edit:540942003504939008>"

    @staticmethod
    def get_steam_emoji():
        return "<:steam:540913793241186334>"

    @staticmethod
    def get_steam_edit_emoji():
        return "<:steam_edit:540944913596940298>"

    @staticmethod
    def get_discord_emoji():
        return "<:discord:525264685851738112>"

    @staticmethod
    def get_discord_edit_emoji():
        return "<:discord_edit:540945043922354196>"

    @staticmethod
    def get_error_emoji():
        return "<:error:540884356739104768>"

    @staticmethod
    def get_lvl_up_emoji():
        return "<:LvlUp:619532916204109825>"

    @staticmethod
    def get_star_player_emoji():
        return "<:StarPlayer:619533822857576479>"

    @staticmethod
    def get_lvl1_emoji():
        return "<:Lvl1:619533881644679179>"

    @staticmethod
    def get_lvl2_emoji():
        return "<:Lvl2:619533881833684994>"

    @staticmethod
    def get_lvl3_emoji():
        return "<:Lvl3:619533882093731840>"

    @staticmethod
    def get_lvl4_emoji():
        return "<:Lvl4:619533882177617950>"

    @staticmethod
    def get_lvl5_emoji():
        return "<:Lvl5:619533882177355776>"

    @staticmethod
    def get_lvl6_emoji():
        return "<:Lvl6:619533882026491904>"

    @staticmethod
    def get_lvl7_emoji():
        return "<:Lvl7:619533882122960909>"

    @staticmethod
    def get_lvl_emoji(level):
        if level == 1:
            return Resources.get_lvl1_emoji()
        elif level == 2:
            return Resources.get_lvl2_emoji()
        elif level == 3:
            return Resources.get_lvl3_emoji()
        elif level == 4:
            return Resources.get_lvl4_emoji()
        elif level == 5:
            return Resources.get_lvl5_emoji()
        elif level == 6:
            return Resources.get_lvl6_emoji()
        elif level == 7:
            return Resources.get_lvl7_emoji()
        return None

    @staticmethod
    def get_flag_emoji(country):
        if country is None:
            return ""

        if country.lower() == "sp":
            return ":flag_es:"
        elif country.lower() == "wls":
            return ":wales:"
        elif country.lower() == "sct":
            return ":scotland:"
        elif country.lower() == "da":
            return ":flag_dk:"
        elif country.lower() == "en":
            return ":england:"
        return ":flag_" + country.lower() + ":"

    @staticmethod
    def get_block_dice(index: int):
        if index == 1:
            return "<:skull:696642316802326548>"
        elif index == 2:
            return "<:skull_pow:696653573601362021>"
        elif index == 3:
            return "<:push:696654570214129744>"
        elif index == 4:
            return "<:push:696654570214129744>"
        elif index == 5:
            return "<:defender_stumbles:696653573391646781>"
        elif index == 6:
            return "<:pow:696653573492310037>"
        return None
