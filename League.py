#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from discord_resources.Utilities import DiscordUtilities
from spike_database.Leagues import Leagues
from spike_database.ResourcesRequest import ResourcesRequest
from spike_exception import InvalidPlatformError, LeagueNotFoundError
from spike_model.League import League
from spike_requester.CyanideApi import CyanideApi
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_translation import Translator

spike_logger = logging.getLogger("spike_logger")


async def get_league(client, ctx, league_name: str, platform: str = "pc"):
    common_db = ResourcesRequest()
    utils = DiscordUtilities(client)
    channel = ctx.message.channel
    league_db = Leagues()
    platform_list = []
    leagues = []
    settings = DiscordGuildSettings(f"{ctx.guild.id}")

    if platform == "all":
        # We assume that multi platform league like Cabalvision have the same name on each platform
        platform = "pc"
        league_data = CyanideApi.get_leagues(league_name, limit=1, platform=platform)

        platform_list.extend(common_db.get_platforms())
    else:
        platform_id = common_db.get_platform_id(platform)
        if platform_id is None:
            raise InvalidPlatformError(settings.get_language(), platform)
        else:
            platform_list.append(platform_id)
            league_data = CyanideApi.get_leagues(league_name, limit=1, platform=platform)

    if league_data is None or league_data["leagues"] is None:
        league_data = CyanideApi.get_league(league_name, platform=platform)
    else:
        found_name = league_data["leagues"][0]["name"]
        league_data = CyanideApi.get_league(found_name, platform=platform)

    if league_data is None or league_data.get("league") is None:
        raise LeagueNotFoundError(settings.get_language(), league_name=league_name, platform=platform)

    league_data = league_data["league"]
    found_name = league_data["name"]
    # /* Would you add **{league_name}** ? */
    question = Translator.tr("#_league_utilities.add_league_first_request", settings.get_language()).format(league_name=found_name)
    # /* yes */
    reply_yes = Translator.tr("#_league_utilities.yes", settings.get_language())
    # /* no */
    reply_no = Translator.tr("#_league_utilities.no", settings.get_language())
    cancel_word = Translator.tr("#_common_command.cancel_word", settings.get_language())
    reply = await utils.ask_string(question, "{}/{}".format(reply_yes, reply_no), cancel_word, channel, ctx.message.author)

    if reply is not None and reply.lower() == reply_no:
        league_data = CyanideApi.get_league(league_name, platform=platform)
        if league_data is not None and league_data["league"] is not None:
            found_name = league_data["league"]["name"]
            # /* Would you add **{league_name}** instead ? */
            question = Translator.tr("#_league_utilities.add_league_second_request", settings.get_language()).format(league_name=found_name)
            reply = await utils.ask_string(question, "{}/{}".format(reply_yes, reply_no), cancel_word, channel, ctx.message.author)
        else:
            raise LeagueNotFoundError(settings.get_language(), league_name, platform)
    else:
        league_data = CyanideApi.get_league(found_name, platform=platform)
        found_name = league_data["league"]["name"]

    if reply is not None and reply.lower() == reply_yes:
        for platform_id in platform_list:
            platform = common_db.get_platform(platform_id)
            current_league_data = CyanideApi.get_league(found_name, platform=platform[1])
            if current_league_data is not None and current_league_data["league"] is not None:
                league_model = League(current_league_data["league"])
                league_model.set_platform_id(platform_id)
                league_model.set_platform(platform[1])
                leagues.append(league_model)
                league_db.add_or_update_raw_league(league_model, platform_id)

    return leagues
