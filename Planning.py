#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime

from pytz import timezone

from spike_database.Contests import Contests
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_translation import Translator
from discord_resources.Utilities import DiscordUtilities


class Planning:

    def __init__(self, client):
        self.client = client

    async def ask_scheduling(self, raw_reaction):
        utils = DiscordUtilities(self.client)
        settings = DiscordGuildSettings(raw_reaction.guild_id)
        user = self.client.get_user(raw_reaction.user_id)
        contest_db = Contests()
        contest_data = contest_db.get_contest_data(raw_reaction.message_id)

        if contest_data is not None:
            channel = self.client.get_channel(raw_reaction.channel_id)
            if channel is not None:
                message = await channel.fetch_message(raw_reaction.message_id)
                if message is not None and len(message.embeds) == 1:
                    original_embed = message.embeds[0]
                    original_description = original_embed.description

                    # /* when you want schedule this match */
                    date_planned, df = await utils.ask_date_time(message, Translator.tr("#_game_scheduling.request_date_time", settings.get_language()), channel, user, self.get_date_formats(), settings.get_timezone())

                    if date_planned is not None and isinstance(date_planned, datetime):
                        description = "{} *{}*".format(date_planned.strftime(df), settings.get_timezone())

                        output_tz = timezone("UTC")
                        date_planned = date_planned.astimezone(output_tz)

                        if "??:??" in df:
                            saved_date = date_planned.strftime("%Y-%m-%d ??:??")
                        elif ":??" in df:
                            saved_date = date_planned.strftime("%Y-%m-%d %H:??")
                        else:
                            saved_date = date_planned.strftime("%Y-%m-%d %H:%M")

                        contest_db.update_contest_date(contest_data["contest_id"], contest_data["platform_id"], saved_date)
                    else:
                        description = original_description

                    for message in contest_data["messages"]:
                        channel_id = message["channel_id"]
                        message_id = message["message_id"]
                        channel = self.client.get_channel(channel_id)
                        if channel is not None:
                            msg = await channel.fetch_message(message_id)
                            if msg is not None:
                                embed = msg.embeds[0]
                                if embed is not None:
                                    embed.description = description
                                    await msg.edit(embed=embed)

    @staticmethod
    def get_date_formats():
        return ["%d/%m ??:??", "%d/%m :: ??:??", "%d/%m : ??:??", "%d/%m::??:??", "%d/%m:??:??", "%d/%m:: ??:??", "%d/%m: ??:??", "%d/%m ::??:??", "%d/%m :??:??",
                "%d/%m %H:??", "%d/%m :: %H:??", "%d/%m : %H:??", "%d/%m::%H:??", "%d/%m:%H:??", "%d/%m:: %H:??", "%d/%m: %H:??", "%d/%m ::%H:??", "%d/%m :%H:??",
                "%d/%m %H:%M", "%d/%m :: %H:%M", "%d/%m : %H:%M", "%d/%m::%H:%M", "%d/%m:%H:%M", "%d/%m:: %H:%M", "%d/%m: %H:%M", "%d/%m ::%H:%M", "%d/%m :%H:%M"]
